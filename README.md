Bitbucket with MathJax (Chrome extension)
=========================================

Extension adds support for LaTeX equations in Bitbucket repositories, issue tracker and comments, rendering them with an open source [MathJax](http://mathjax.org/) library. For example, if you have it installed you should see the following lovely equation: $e^{i \pi} + 1 = 0$.

Right-Click on equation to show MathJax's context menu with additional options, e.g. "Scale All Math..." to instantly scale all equations on a page, "TeX commands" to see the source TeX equation etc.

Extension is published under [New BSD License](https://bitbucket.org/kgusev/bitbucket-mathjax) with the source code available [here](https://bitbucket.org/kgusev/bitbucket-mathjax). This extension is based on the __wonderful__ [github-mathjax](https://github.com/orsharir/github-mathjax) extension by [Or Sharir](https://github.com/orsharir), and its license is included as part of LICENSE.md in this repository. The icons are courtesy of [IconArchive](http://www.iconarchive.com/show/flat-gradient-social-icons-by-limav/Bitbucket-icon.html).

For bug reports and feature requests, please use [Issue tracker](https://bitbucket.org/kgusev/bitbucket-mathjax).

### INSTALLATION:

Official release available at [Chrome Web Store](https://chrome.google.com/webstore/detail/bitbucket-with-mathjax/afohachalomejebelclfpflmimemiadm).
